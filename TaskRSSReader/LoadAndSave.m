//
//  LoadAndSave.m
//  TaskRSSReader
//
//  Created by Igor Rozhnev  on 26.01.16.
//  Copyright © 2016 Igor Rozhnev. All rights reserved.
//

#import "LoadAndSave.h"

@implementation LoadAndSave

+ (id)loadObjectFromFile:(NSString *)file{
    NSString *filePath = [docPath stringByAppendingPathComponent:file];
    NSData *data = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    return data;
}

+ (void)saveObjectToFile:(NSString *)file object:(id)object{
    NSString *filePath = [docPath stringByAppendingPathComponent:file];
    [NSKeyedArchiver archiveRootObject:object toFile:filePath];
}

@end
