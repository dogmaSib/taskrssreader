//
//  main.m
//  TaskRSSReader
//
//  Created by Igor Rozhnev on 26.01.16.
//  Copyright © 2016 Igor Rozhnev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
