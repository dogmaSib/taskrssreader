//
//  LinksRSSCell.m
//  TaskRSSReader
//
//  Created by Igor Rozhnev on 01.02.16.
//  Copyright © 2016 Igor Rozhnev. All rights reserved.
//

#import "LinksRSSCell.h"
@interface LinksRSSCell()

@property (weak, nonatomic) IBOutlet UILabel *labelLinkRSS;

@end

@implementation LinksRSSCell

- (void)awakeFromNib {
    // Initialization code
}


-(void) setTitle:(NSString *)title{
    _title = title;
    
    self.labelLinkRSS.text = title;
}

@end
