//
//  Constants.h
//  TaskRSSReader
//
//  Created by Igor Rozhnev on 27.01.16.
//  Copyright © 2016 Igor Rozhnev. All rights reserved.
//

#import <Foundation/Foundation.h>

#define docPath [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]

extern NSString *const keyItem;
extern NSString *const keyTitle;
extern NSString *const keyPubDate;
extern NSString *const keyDescription;
extern NSString *const keyLink;

extern NSString *const keyUpdateNotification;
extern NSString *const keyGetLinks;
extern NSString *const keyAddNewRSSLink;
extern NSString *const keyEditRSSLink;

extern NSString *const fileRSSLinks;
extern NSString *const fileRSSLentsContent;
