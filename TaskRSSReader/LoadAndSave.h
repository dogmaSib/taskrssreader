//
//  LoadAndSave.h
//  TaskRSSReader
//
//  Created by Igor Rozhnev  on 26.01.16.
//  Copyright © 2016 Igor Rozhnev. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface LoadAndSave : NSObject

+ (id)loadObjectFromFile:(NSString *)file;
+ (void)saveObjectToFile:(NSString *)file object:(id)object;


@end
