//
//  RSSLenta.m
//  TaskRSSReader
//
//  Created by Igor Rozhnev on 26.01.16.
//  Copyright © 2016 Igor Rozhnev. All rights reserved.
//

NSString *const keyRSSLentaTitle = @"rssLentaTitle";
NSString *const keyRSSLentaDate = @"rssLentaDate";
NSString *const keyRSSLentaArticles = @"rssLentaArticles";

#import "RSSLenta.h"

@implementation RSSLenta

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.title forKey:keyRSSLentaTitle];
    [aCoder encodeObject:self.dateOfDownload forKey:keyRSSLentaDate];
    [aCoder encodeObject:self.articles forKey:keyRSSLentaArticles];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super init]){
        _title = [aDecoder decodeObjectForKey:keyRSSLentaTitle];
        _dateOfDownload = [aDecoder decodeObjectForKey:keyRSSLentaDate];
        _articles = [aDecoder decodeObjectForKey:keyRSSLentaArticles];
    }
    return self;
}

@end
