//
//  ArticleCell.h
//  TaskRSSReader
//
//  Created by Igor Rozhnev on 27.01.16.
//  Copyright © 2016 Igor Rozhnev. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Article.h"

@interface ArticleCell : UITableViewCell

@property (nonatomic, strong) Article *article;

@end
