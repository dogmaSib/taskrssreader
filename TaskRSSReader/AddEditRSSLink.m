//
//  AddEditRSSLink.m
//  TaskRSSReader
//
//  Created by Igor Rozhnev on 01.02.16.
//  Copyright © 2016 Igor Rozhnev. All rights reserved.
//

#import "AddEditRSSLink.h"
#import "LoadAndSave.h"


@interface AddEditRSSLink ()

@property (weak, nonatomic) IBOutlet UITextField *textFieldLinkRSS;
@property (nonatomic) BOOL modeEdit;

@end

@implementation AddEditRSSLink

- (void)viewDidLoad {
    [super viewDidLoad];
    if(self.linkRSS){
        self.modeEdit = YES;
        self.textFieldLinkRSS.text = self.linkRSS;
        self.navigationItem.title = @"Edit";
    }
    else{
        self.navigationItem.title = @"Add New";
    }
    // Do any additional setup after loading the view.
}

- (IBAction)savingCurrentLink:(id)sender {
    if (self.modeEdit) {
        NSMutableArray *links = [LoadAndSave loadObjectFromFile:fileRSSLinks];
        if (![self.linkRSS isEqualToString:self.textFieldLinkRSS.text]) {
            NSInteger index = [links indexOfObject:self.linkRSS];
            [links replaceObjectAtIndex:index withObject:self.textFieldLinkRSS.text];
            [LoadAndSave saveObjectToFile:fileRSSLinks object:links];
            [[NSNotificationCenter defaultCenter] postNotificationName:keyEditRSSLink object:nil];
        }
        [self.navigationController popViewControllerAnimated:YES];        
    }
    else{
        if (self.textFieldLinkRSS.text.length) {
            [[NSNotificationCenter defaultCenter] postNotificationName:keyAddNewRSSLink object:self.textFieldLinkRSS.text];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else{
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Input url RSS" preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
        }
    }
    
    
}

@end
