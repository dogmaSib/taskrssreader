//
//  ArticleCell.m
//  TaskRSSReader
//
//  Created by Igor Rozhnev on 27.01.16.
//  Copyright © 2016 Igor Rozhnev. All rights reserved.
//

#import "ArticleCell.h"

@interface ArticleCell()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end

@implementation ArticleCell

- (void)awakeFromNib {
    
}

- (void)setArticle:(Article *)article{
    _article = article;
    self.titleLabel.text = article.title;
    self.descriptionLabel.text = article.content;
}

@end
