//
//  LinksRSSCell.h
//  TaskRSSReader
//
//  Created by Igor Rozhnev on 01.02.16.
//  Copyright © 2016 Igor Rozhnev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LinksRSSCell : UITableViewCell

@property (nonatomic,strong) NSString *title;

@end
