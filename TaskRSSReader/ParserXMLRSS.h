//
//  ParserXMLRSS.h
//  TaskRSSReader
//
//  Created by Igor Rozhnev on 26.01.16.
//  Copyright © 2016 Igor Rozhnev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ParserXMLRSS : NSObject<NSXMLParserDelegate>

- (instancetype)init;

- (void)loadRSSWithUrl:(NSString *)url;

@end
