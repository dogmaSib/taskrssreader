//
//  ListRsSLentsController.m
//  TaskRSSReader
//
//  Created by Igor Rozhnev on 27.01.16.
//  Copyright © 2016 Igor Rozhnev. All rights reserved.
//

#import "ListRSSLentsView.h"

#import "EditLinksToRssView.h"

#import "ArticleCell.h"
#import "LoadAndSave.h"
#import "ParserXMLRSS.h"
#import "RSSLenta.h"
#import "Reachability.h"

#import "NewsDetailView.h"


#define identifierKeyCell @"ArticleCell"

@interface ListRSSLentsView ()


@property (nonatomic, strong) NSArray *links;
@property (nonatomic, strong) ParserXMLRSS *parser;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) Reachability *internetReachability;
@property (nonatomic) BOOL hasConnect;

@end

@implementation ListRSSLentsView

- (void)viewDidLoad {
    [super viewDidLoad];
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self
                            action:@selector(updateContent)
                  forControlEvents:UIControlEventValueChanged];
    

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    self.internetReachability = [Reachability reachabilityForInternetConnection];   //Initialization inspection test internet
    [self.internetReachability startNotifier];
    NetworkStatus netStatus = [self.internetReachability currentReachabilityStatus];
    self.hasConnect = netStatus == NotReachable ? NO : YES;

    UINib* nib = [UINib nibWithNibName:identifierKeyCell bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:identifierKeyCell];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(insertRSSLenta:) name:keyGetLinks object:nil];
    self.navigationItem.title = @"RSS Lents";
    self.items = [NSMutableArray new];
    self.parser = [[ParserXMLRSS alloc] init];
    [self updateContent];
}

- (void)reachabilityChanged:(NSNotification *)note
{
    Reachability* reachability = note.object;
    if (reachability == self.internetReachability) {
        NetworkStatus netStatus = [reachability currentReachabilityStatus];
        self.hasConnect = netStatus == NotReachable ? NO : YES;
        if (netStatus == NotReachable) {
            //alert window if connection lost
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Lost intrernet connection" preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    
}

- (void)updateContent{
    if (self.items.count)
        [self.items removeAllObjects];
  
    self.links = [LoadAndSave loadObjectFromFile:fileRSSLinks];
    if (self.hasConnect) {
        //if has connect parsing resources
        NSOperationQueue *queue = [NSOperationQueue new];
        queue.maxConcurrentOperationCount = 1;
        
        NSMutableArray *operations = [NSMutableArray new];
        NSString *filePath = [docPath stringByAppendingPathComponent:fileRSSLentsContent];
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            NSError *error;
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
        }
        for (NSString *link in self.links) {
            NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(loadRSSWithUrl:) object:link];
            [operations addObject:operation];
        }
        //adding to the queue
        [queue addOperations:operations waitUntilFinished:YES];
    }
    else{
        //else get information from cache
        self.items = [LoadAndSave loadObjectFromFile:fileRSSLentsContent];
        if(!self.items)
            self.items = [NSMutableArray new];
        [self.tableView reloadData];
        [self.refreshControl endRefreshing];
    }
    
}

- (void)loadRSSWithUrl:(NSString *)url{
    [self.parser loadRSSWithUrl:url];
}

- (void)insertRSSLenta:(NSNotification *)notification{
    RSSLenta *rssLenta = (RSSLenta *)notification.object;
    dispatch_async(dispatch_get_main_queue(),^ {
        //out of the flow line and the addition of a new section
        [self.tableView beginUpdates];
        [self.items addObject:rssLenta];
        [self.tableView insertSections:[NSIndexSet indexSetWithIndex:self.items.count - 1] withRowAnimation:UITableViewRowAnimationLeft];
        [self.tableView endUpdates];
        [self.refreshControl endRefreshing];
    });
    
}

#pragma mark - Table view data source
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.items.count;
}
-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    RSSLenta *rssLenta = self.items[section];
    return rssLenta.title;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    RSSLenta *rssLenta = self.items[section];
    return rssLenta.articles.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 98;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ArticleCell *cell = [tableView dequeueReusableCellWithIdentifier:identifierKeyCell forIndexPath:indexPath];
    RSSLenta *rssLenta = self.items[indexPath.section];
    Article *article = rssLenta.articles[indexPath.row];
    cell.article = article;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RSSLenta *rssLenta = self.items[indexPath.section];
    Article *article = rssLenta.articles[indexPath.row];
    [self performSegueWithIdentifier:@"detailArticle" sender:article];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"detailArticle"]) {
        NewsDetailView *controller = segue.destinationViewController;
        controller.article = sender;
    }
}

@end
