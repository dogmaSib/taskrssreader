//
//  Article+Parsing.h
//  TaskRSSReader
//
//  Created by Igor Rozhnev on 26.01.16.
//  Copyright © 2016 Igor Rozhnev. All rights reserved.
//

#import "Article.h"

@interface Article (Parsing)

- (instancetype)initWithValues:(NSDictionary *)values;

@end
