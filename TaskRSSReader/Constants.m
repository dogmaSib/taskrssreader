//
//  Constants.m
//  TaskRSSReader
//
//  Created by Igor Rozhnev on 27.01.16.
//  Copyright © 2016 Igor Rozhnev. All rights reserved.
//

#import "Constants.h"

NSString *const keyItem = @"item";
NSString *const keyTitle = @"title";
NSString *const keyPubDate = @"pubDate";
NSString *const keyDescription = @"description";
NSString *const keyLink = @"link";

NSString *const keyUpdateNotification = @"updateContentNotification";
NSString *const keyGetLinks = @"getNewsFromURLS";
NSString *const fileRSSLinks = @"fileRSSLinks";
NSString *const keyAddNewRSSLink = @"addNewRSSLink";
NSString *const keyEditRSSLink = @"editRSSLink";
NSString *const fileRSSLentsContent = @"fileRSSLentsContent";
