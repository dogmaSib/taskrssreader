//
//  ParserXMLRSS.m
//  TaskRSSReader
//
//  Created by Igor Rozhnev on 26.01.16.
//  Copyright © 2016 Igor Rozhnev. All rights reserved.
//

#import "ParserXMLRSS.h"
#import "Article+Parsing.h"
#import "LoadAndSave.h"
#import "RSSLenta.h"
#import "LoadAndSave.h"

@interface ParserXMLRSS()

@property (nonatomic, strong) NSXMLParser *parser;
@property (nonatomic, strong) NSString *currentElement;
@property (nonatomic, strong) NSMutableString *temp;
@property (nonatomic, strong) NSSet *allKeys;
@property (nonatomic, strong) NSMutableDictionary *dict;
@property (nonatomic, strong) NSMutableArray *tempArticles;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) RSSLenta *rssLenta;

@end

@implementation ParserXMLRSS

- (instancetype)init{
    if(self = [super init]){
        self.allKeys = [NSSet setWithObjects:keyTitle, keyPubDate, keyLink, keyDescription, nil];
    }
    return self;
}

- (void)loadRSSWithUrl:(NSString *)url{
    self.url = url;
    self.tempArticles = [NSMutableArray new];
    self.temp = [NSMutableString new];
    self.rssLenta = [RSSLenta new];
    self.rssLenta.title = url;
    self.parser = [[NSXMLParser alloc] initWithContentsOfURL:[NSURL URLWithString:url]];
    self.parser.delegate = self;
    [self.parser parse];

    
}

#pragma mark - NSXMLParser Delegate

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    self.currentElement = elementName;
    if ([elementName isEqualToString:keyItem]) {
        self.dict = [NSMutableDictionary new];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string;{   //finding nested elements and assigning tag values
    if ([self.allKeys containsObject:self.currentElement]) {
        if (![string isEqualToString:@"\n"]) {
            [self.temp appendString:string];
        }
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName;{
    if ([elementName isEqualToString:keyItem]) { //  parsing dictionary and creating object
        Article *article = [[Article alloc] initWithValues:self.dict];
        [self.tempArticles addObject:article];
    }
    else if ([self.allKeys containsObject:elementName]) {
        [self.dict setObject:self.temp forKey:elementName];
        self.temp = [NSMutableString new];
    }
}
- (void)parserDidEndDocument:(NSXMLParser *)parser {
    self.rssLenta.articles = self.tempArticles;
    self.rssLenta.dateOfDownload = [NSDate date];
    NSMutableArray *lents = [LoadAndSave loadObjectFromFile:fileRSSLentsContent];
    if (!lents) {
        lents = [NSMutableArray new];
    }
    [lents addObject:self.rssLenta];
    [LoadAndSave saveObjectToFile:fileRSSLentsContent object:lents];
    [[NSNotificationCenter defaultCenter] postNotificationName:keyGetLinks object:self.rssLenta];//refer the notification to the ListRSSLentsView controller
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {        //if the connection is interrupted by a dialog box
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
//                                                    message:@"No network connection"
//                                                   delegate:nil
//                                          cancelButtonTitle:@"OK"
//                                          otherButtonTitles:nil];
//    [alert show];
}

@end
