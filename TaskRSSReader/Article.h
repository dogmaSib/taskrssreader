//
//  Article.h
//  TaskRSSReader
//
//  Created by Igor Rozhnev on 26.01.16.
//  Copyright © 2016 Igor Rozhnev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Article : NSObject<NSCoding>

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *link;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *imgURL;
@property (nonatomic, strong) NSString *dateNew;

@end
