//
//  NewsDetailView.h
//  TaskRSSReader
//
//  Created by Igor Rozhnev on 31.01.16.
//  Copyright © 2016 Igor Rozhnev. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Article;
@interface NewsDetailView : UIViewController

@property (nonatomic, strong) Article *article;

@end
