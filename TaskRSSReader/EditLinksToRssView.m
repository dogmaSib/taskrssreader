//
//  EditLinksToRssView.m
//  TaskRSSReader
//
//  Created by Igor Rozhnev on 31.01.16.
//  Copyright © 2016 Igor Rozhnev. All rights reserved.
//

#import "EditLinksToRssView.h"
#import "LinksRSSCell.h"
#import "LoadAndSave.h"
#import "AddEditRSSLink.h"


#define identifierCell @"LinksRSSCell"

@interface EditLinksToRssView ()

@property (nonatomic, strong) NSMutableArray *linksRSS;

@end

@implementation EditLinksToRssView

- (void)viewDidLoad {
    [super viewDidLoad];
    UINib* nib = [UINib nibWithNibName:identifierCell bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:identifierCell];
    
    self.linksRSS = [LoadAndSave loadObjectFromFile:fileRSSLinks];
    if (!self.linksRSS)
        self.linksRSS = [NSMutableArray new];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateWithLinkRSS:) name:keyAddNewRSSLink object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateContent) name:keyEditRSSLink object:nil];
}

- (void)updateContent{
    self.linksRSS = [LoadAndSave loadObjectFromFile:fileRSSLinks];
    [self.tableView reloadData];
}

- (void)updateWithLinkRSS:(NSNotification *)notification{
    NSString *url = notification.object;
    [self.linksRSS addObject:url];
    [LoadAndSave saveObjectToFile:fileRSSLinks object:self.linksRSS];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.linksRSS count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LinksRSSCell *cell = [tableView dequeueReusableCellWithIdentifier:identifierCell forIndexPath:indexPath];
    cell.title = self.linksRSS[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"addEdit" sender:self.linksRSS[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.linksRSS removeObjectAtIndex:indexPath.row];
        [LoadAndSave saveObjectToFile:fileRSSLinks object:self.linksRSS];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"addEdit"]) {
        AddEditRSSLink *controller = segue.destinationViewController;
        controller.linkRSS = sender;
    }
}

- (IBAction)addNewRSSLink:(id)sender {
    [self performSegueWithIdentifier:@"addEdit" sender:nil];
}

@end
