//
//  NewsDetailView.m
//  TaskRSSReader
//
//  Created by Igor Rozhnev on 31.01.16.
//  Copyright © 2016 Igor Rozhnev. All rights reserved.
//

#import "NewsDetailView.h"

#import "Article.h"

@interface NewsDetailView ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end

@implementation NewsDetailView

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Статья";
    self.titleLabel.text = self.article.title;
    self.descriptionLabel.text = self.article.content;
    self.dateLabel.text = self.article.dateNew;
}


@end
