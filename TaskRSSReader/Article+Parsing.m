//
//  Article+Parsing.m
//  TaskRSSReader
//
//  Created by Igor Rozhnev on 26.01.16.
//  Copyright © 2016 Igor Rozhnev. All rights reserved.
//

#import "Article+Parsing.h"

@implementation Article (Parsing)

- (instancetype)initWithValues:(NSDictionary *)values{
    if (self = [super init]) {
        self.title = values[keyTitle];
        self.link = values[keyLink];
        self.content = values[keyDescription];
        self.dateNew = values[keyPubDate];
    }
    return self;
}

@end
