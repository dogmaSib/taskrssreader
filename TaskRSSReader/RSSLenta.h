//
//  RSSLenta.h
//  TaskRSSReader
//
//  Created by Igor Rozhnev on 26.01.16.
//  Copyright © 2016 Igor Rozhnev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Article.h"

@interface RSSLenta : NSObject <NSCoding>

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSDate *dateOfDownload;
@property (nonatomic, strong) NSArray *articles;

@end
