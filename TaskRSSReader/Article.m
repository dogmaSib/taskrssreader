//
//  Article.m
//  TaskRSSReader
//
//  Created by Igor Rozhnev on 26.01.16.
//  Copyright © 2016 Igor Rozhnev. All rights reserved.
//

NSString *const keyArticleTitle = @"articleTitle";
NSString *const keyArticleLink = @"articleLink";
NSString *const keyArticleContent = @"articleContent";
NSString *const keyArticleImg = @"articleImg";
NSString *const keyArticleDate = @"articleDate";

#import "Article.h"

@implementation Article

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.title forKey:keyArticleTitle];
    [aCoder encodeObject:self.link forKey:keyArticleLink];
    [aCoder encodeObject:self.content forKey:keyArticleContent];
//    [aCoder encodeObject:self.imgURL forKey:keyArticleImg];
    [aCoder encodeObject:self.dateNew forKey:keyArticleDate];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]) {
        _title = [aDecoder decodeObjectForKey:keyArticleTitle];
        _link = [aDecoder decodeObjectForKey:keyArticleLink];
        _content = [aDecoder decodeObjectForKey:keyArticleContent];
//        _imgURL = [aDecoder decodeObjectForKey:keyArticleImg];
        _dateNew = [aDecoder decodeObjectForKey:keyArticleDate];
    }
    return self;
}

@end
